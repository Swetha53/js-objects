
var inverted =[]
function invert(obj) {
const key = Object.keys(obj);
  
  const values = Object.values(obj);
 

for ( let i = 0; i < key.length; i++) {
    inverted[i] = [values[i],key[i]];
  };

const output =inverted

  .reduce((inverted, [keys, values]) => {
    inverted[keys] = values;
    return inverted;
  },{});
return output
}


module.exports=invert